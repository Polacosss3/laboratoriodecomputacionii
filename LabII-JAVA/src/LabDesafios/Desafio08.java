package LabDesafios;

import javax.swing.*;
import java.text.DecimalFormat;

public class Desafio08 {
    public static void main (String[] args){

        int n;
        n = Integer.parseInt(JOptionPane.showInputDialog("Introduce un número"));
        while(n <= 0){
            n = Integer.parseInt(JOptionPane.showInputDialog("No se puede obtener la raiz de 0 o un numero negativo, ingrese nuevamente."));
        }
        DecimalFormat df = new DecimalFormat("#.00");
        JOptionPane.showMessageDialog(null, "La raíz de "+ n +" es: "+(df.format(Math.sqrt(n))));



    }
}









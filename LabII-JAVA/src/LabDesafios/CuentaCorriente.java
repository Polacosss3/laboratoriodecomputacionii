import java.util.Scanner;
import java.util.Random;

public class CuentaCorriente {

    private double saldo = 999.9;
    private String nTitular = "Franco Santiano";
    Random random = new Random();
    private long nCuenta=random.nextLong();

    public CuentaCorriente(String nTitular){
        this.saldo = 0;
        this.nTitular = nTitular;
        if (nCuenta<0){
            nCuenta=nCuenta*(-1);
        }
    }
    public CuentaCorriente(double saldo, String nTitular) {
        this.saldo = saldo;
        this.nTitular = nTitular;
        if (nCuenta<0){
            nCuenta=nCuenta*(-1);
        }
    }

    public double getSaldo() {
        return saldo;
    }

    public long getNumeroCuenta() {
        return nCuenta;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public String getNombreTitular() {
        return nTitular;
    }

    public void setNombreTitular(String nTitular) {
        this.nTitular = nTitular;
    }


    public void ingresarDinero(int dinero){
        if (dinero>0){
            this.saldo+=dinero;
        } else System.out.println("No se pudo realizar la operacion.");
    }
    public void sacarDinero(int dinero){
        if(this.saldo<dinero){
            System.out.println("Operacion no completada. Su saldo no es suficiente para realizar el retiro");
        } else this.saldo-=dinero;
    }


    public static void transferencia(CuentaCorriente Cuenta1, CuentaCorriente Cuenta2){
        double importe;
        System.out.println("Ingrese el importe que desea para la transferencia");
        Scanner sc = new Scanner(System.in);
        importe=sc.nextDouble();
        Cuenta1.setSaldo(Cuenta1.getSaldo()-importe);
        Cuenta2.setSaldo(Cuenta2.getSaldo()+importe);
        System.out.println("Cuenta 1: "+ "\n" + Cuenta1.getNombreTitular()+"\n"+Cuenta1.getSaldo());
        System.out.println("Cuenta 2: "+ "\n" + Cuenta2.getNombreTitular()+"\n"+Cuenta2.getSaldo());
    }


}



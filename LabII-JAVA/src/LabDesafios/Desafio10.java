package LabDesafios;
import java.util.Scanner;

public class Desafio10 {
    public static void main (String[] args) {

        int intentos = 0;
        int introd;
        int random = (int) Math.floor(Math.random() * 100 + 1);

        System.out.print("Proceda a acertar el numero aleatorio mayor a 0 y menor a 100: ");
        Scanner nu = new Scanner(System.in);
        introd = nu.nextInt();

        while (random != introd) {
            if (introd > random) {
                intentos++;
                System.out.print("El numero ingresado es mayor al numero aleatorio, intenta nuevamente:");
                introd = nu.nextInt();
            } else {
                intentos++;
                System.out.print("El numero ingresado es menor al numero aleatorio, intenta nuevamente:");
                introd = nu.nextInt();
            }
        }
        if (random == introd) {
            intentos++;
            System.out.print("Lo has conseguido, acertaste el numero en: " + intentos + " intentos");
        }

    }
    }



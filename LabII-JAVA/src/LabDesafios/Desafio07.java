package LabDesafios;

public class Desafio07 {
    public static void main (String[] args) {

        double y = Math.toRadians(57);
        double C1 = 43.2;
        double C2 = 80.14;

    System.out.println("***Funciones trigonometricas habituales***");
    System.out.printf("Coseno de 57: %f\n", Math.cos(y));
    System.out.printf("Seno de 57: %f\n", Math.sin(y));
    System.out.printf("Tangente de 57: %f\n", Math.tan(y));
    System.out.printf("Arcotangente de 57: %f\n", Math.atan(Math.toDegrees(y)));
    System.out.printf("Coordenadas(43,2;80,14): %f\n", Math.atan2(C1,C2));

        System.out.println("\n***Funcion Exponencial y su inversa***");
        System.out.println("Exponente de 57: %d"+Math.exp(y));
        System.out.println("Logaritmo de 57: %d" + Math.log(y));

        System.out.println("\n***Constantes PI y e***");
        System.out.println("La constante PI es: "+ Math.PI);
        System.out.println("Constante e: "+ Math.E);
}
}

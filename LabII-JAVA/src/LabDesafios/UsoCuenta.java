public class UsoCuenta extends CuentaCorriente{
    static CuentaCorriente Cuenta1 = new CuentaCorriente(20000.1,"Lilita Michelin");
    static CuentaCorriente Cuenta2= new CuentaCorriente(6000.25,"Jose Perez");

    public UsoCuenta(String nTitular) {
        super(nTitular);
    }

    public UsoCuenta(double saldo, String nTitular) {
        super(saldo, nTitular);
    }

    public static void main(String[] args) {
        System.out.println(Cuenta1.getNumeroCuenta());
        transferencia(Cuenta1,Cuenta2);
    }

}
